package poste;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class TestColisExpress {
	
private static float tolerancePrix=0.001f;
	
	ColisExpress colisExpress1;
	
	@BeforeEach
	void init() {
		try {
			colisExpress1 = new ColisExpress("Le pere Noel", 
					"famille Kaya, igloo 10, terres ouest",
					"7877", 24, 0.02f, Recommandation.deux, "train electrique", 200, true);
		}
		catch(ColisExpressInvalide e) {
			// Maybe do something
		}
	}
	
	
	@Test
	void testColisExpressToString() {
		assertAll("toString correct",
				()-> assertEquals(
						"Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/24.0/1",
						colisExpress1.toString ()
					)
				);
	}
	
	
	@Test
	void testColisExpressAffranchissement() {
		assertAll("tarifAffranchissement correct",
				()-> assertEquals(
						33.0f,
						colisExpress1.tarifAffranchissement(),
						tolerancePrix
					)
				);
	}
	
	
	@Test
	void testColisExpressRemboursement() {
		assertAll("tarifRemboursement correct",
				()-> assertEquals(
						100.0f,
						colisExpress1.tarifRemboursement(),
						tolerancePrix
					)
				);
	}
	
	@Test
	void testColisInvalideException() {
		assertThrows(ColisExpressInvalide.class, 
				() -> new ColisExpress(
						"Le pere Noel", 
						"famille Kaya, igloo 10, terres ouest",
						"7877", 35, 0.02f, Recommandation.deux, "train electrique", 200, true
						));
	}
	
	
	
	
	
}