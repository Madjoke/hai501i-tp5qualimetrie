package poste;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class TestColis {
	
	private static float tolerancePrix=0.001f;
	
	Colis colis1;
	
	@BeforeEach
	void init() {
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
	
	@Test
	void testColisToString() {
		assertAll("toString correct",
				()-> assertEquals(
						"Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0",
						colis1.toString ()
					)
				);
	}
	
	
	@Test
	void testColisAffranchissement() {
		assertAll("tarifAffranchissement correct",
				()-> assertEquals(
						3.5f,
						colis1.tarifAffranchissement(),
						tolerancePrix
					)
				);
	}
	
	
	@Test
	void testColisRemboursement() {
		assertAll("tarifRemboursement correct",
				()-> assertEquals(
						100.0f,
						colis1.tarifRemboursement(),
						tolerancePrix
					)
				);
	}
	
}