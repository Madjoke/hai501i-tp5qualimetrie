package poste;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;


public class TestSacPostal {
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	SacPostal sac1;
	Lettre lettre1;
	Lettre lettre2;
	Colis colis1;

	
	@BeforeEach
	void init() {
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		
		sac1 = new SacPostal();
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
	}
	
	
	@Test
	void testValeurRemboursement() {
		assertAll("valeurRemboursement correct",
				()-> assertEquals(
						116.5f,
						sac1.valeurRemboursement(),
						tolerancePrix
					)
				);
	}
	
	@Test
	void testToString() {
		assertEquals(sac1.toString(), "Sac \ncapacite: 0.5\nvolume: 0.025359999558422715\n[Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire, Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence, Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0]\n");
	}
	
	
	@Test
	void testGetVolume() {
		assertAll("getVolume correct",
				()-> assertEquals(
						0.025359999558422715f,
						sac1.getVolume(),
						toleranceVolume
					),
				()-> assertEquals(
						0.02517999955569394f,
						sac1.extraireV1("7877").getVolume(),
						toleranceVolume
					)
				);
	}
	
}